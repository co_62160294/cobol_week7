       IDENTIFICATION DIVISION. 
       PROGRAM-ID. MY-GRADE.
       AUTHOR. Puchong.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT GRADE-FILE ASSIGN TO "mygrade.txt"
           ORGANIZATION IS LINE SEQUENTIAL.
           SELECT AVG-GRADE-FILE ASSIGN TO "avg-grade.txt"
           ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION. 
       FILE SECTION.
       FD GRADE-FILE.
       01  GRADE-DETAIL.
           88 END-OF-GRADE-FILE  VALUE HIGH-VALUE.
           05 ID-COURSE.
              10 FIRST-ID    PIC   X.
              10 SECOND-ID    PIC   X.
              10 ID-COURSE-ALL  PIC   X(4).
           05 NAME-COURSE    PIC   X(50).
           05 CREDIT-COURSE  PIC   9.
           05 GRADE-COURSE   PIC   X(2).
           05 GRADE-NUM      PIC   9V9.

       FD AVG-GRADE-FILE.
       01  AVG-GRADE-DETAIL.
           05 AVG-GRADE-NAME    PIC   X(20).
           05 AVG-GRADE         PIC   9.999.

       WORKING-STORAGE SECTION. 
       01  CAL-GRADE   PIC 9(3)V99.
       01  ALL-CREDIT  PIC 9(3).
       01  AVG-GRADE   PIC 9V999.
       01  CAL-SCI-GRADE   PIC 9(3)V99.
       01  ALL-SCI-CREDIT  PIC 9(3).
       01  AVG-SCI-GRADE   PIC 9V999.
       01  CAL-CS-GRADE   PIC 9(3)V99.
       01  ALL-CS-CREDIT  PIC 9(3).
       01  AVG-CS-GRADE   PIC 9V999.
       

       PROCEDURE DIVISION.
       BEGIN.
           OPEN INPUT GRADE-FILE
           
           PERFORM UNTIL END-OF-GRADE-FILE 
              READ GRADE-FILE 
                 AT END SET END-OF-GRADE-FILE TO TRUE
              END-READ
              IF NOT END-OF-GRADE-FILE THEN
                 PERFORM 001-GRADE-TO-NUM THRU 001-EXIT
                 PERFORM 002-AVG-GRADE THRU 002-EXIT
                 PERFORM 003-AVG-SCI-GRADE THRU 003-EXIT
           END-PERFORM

           PERFORM 005-PRINT THRU 005-EXIT
           PERFORM 006-WRITE THRU 006-EXIT

           CLOSE GRADE-FILE 
           GOBACK 
       .

       001-GRADE-TO-NUM.
           
           EVALUATE TRUE 
              WHEN GRADE-COURSE = "A"  MOVE 4   TO GRADE-NUM
              WHEN GRADE-COURSE = "B+" MOVE 3.5 TO GRADE-NUM
              WHEN GRADE-COURSE = "B"  MOVE 3   TO GRADE-NUM
              WHEN GRADE-COURSE = "C+" MOVE 2.5 TO GRADE-NUM
              WHEN GRADE-COURSE = "C"  MOVE 2   TO GRADE-NUM
              WHEN GRADE-COURSE = "D+" MOVE 1.5 TO GRADE-NUM
              WHEN GRADE-COURSE = "D"  MOVE 1   TO GRADE-NUM
              WHEN OTHER MOVE 0 TO GRADE-NUM  
           END-EVALUATE 
           
           .
       001-EXIT.
           EXIT.

       002-AVG-GRADE.
           COMPUTE CAL-GRADE = CAL-GRADE + (GRADE-NUM * CREDIT-COURSE)
           COMPUTE ALL-CREDIT = ALL-CREDIT + CREDIT-COURSE 
           COMPUTE AVG-GRADE IN AVG-GRADE-DETAIL = 
           CAL-GRADE / ALL-CREDIT 

           .
       002-EXIT.
           EXIT.

       003-AVG-SCI-GRADE.
           IF FIRST-ID = "3" THEN
              IF SECOND-ID = "1" THEN
                 PERFORM 004-AVG-CS-GRADE THRU 004-EXIT 
              ELSE 
                 COMPUTE CAL-SCI-GRADE = CAL-SCI-GRADE + 
                 (GRADE-NUM * CREDIT-COURSE)
                 COMPUTE ALL-SCI-CREDIT = ALL-SCI-CREDIT + CREDIT-COURSE 

                 COMPUTE AVG-SCI-GRADE = CAL-SCI-GRADE / ALL-SCI-CREDIT 

              END-IF 
           END-IF 
           .
       003-EXIT.
           EXIT.

       004-AVG-CS-GRADE.
           COMPUTE CAL-CS-GRADE = CAL-CS-GRADE + 
                 (GRADE-NUM * CREDIT-COURSE)
                 COMPUTE ALL-CS-CREDIT = ALL-CS-CREDIT + CREDIT-COURSE 

                 COMPUTE AVG-CS-GRADE = CAL-CS-GRADE / ALL-CS-CREDIT 

           .
       004-EXIT.
           EXIT.

       005-PRINT.

           DISPLAY "============================================="
           DISPLAY "AVG-GRADE"
           DISPLAY "GRADE: " CAL-GRADE
           DISPLAY "CREDIT: " ALL-CREDIT
           DISPLAY "AVG-GRADE: " AVG-GRADE IN AVG-GRADE-DETAIL  
           DISPLAY "============================================="
           DISPLAY "AVG-SCI-GRADE"
           DISPLAY "SCI-GRADE: " CAL-SCI-GRADE
           DISPLAY "SCI-CREDIT: " ALL-SCI-CREDIT
           DISPLAY "AVG-SCI-GRADE: " AVG-SCI-GRADE 
           DISPLAY "============================================="
           DISPLAY "AVG-CS-GRADE"
           DISPLAY "CS-GRADE: " CAL-CS-GRADE
           DISPLAY "CS-CREDIT: " ALL-CS-CREDIT
           DISPLAY "AVG-CS-GRADE: " AVG-CS-GRADE 
           DISPLAY "============================================="
           
       .

       005-EXIT.
           EXIT.

       006-WRITE.
           OPEN OUTPUT AVG-GRADE-FILE
           MOVE "AVG-GRADE: " TO AVG-GRADE-NAME
           MOVE AVG-GRADE IN AVG-GRADE-DETAIL TO 
           AVG-GRADE IN AVG-GRADE-DETAIL 
           WRITE AVG-GRADE-DETAIL 

           MOVE "AVG-SCI-GRADE: " TO AVG-GRADE-NAME
           MOVE AVG-SCI-GRADE TO AVG-GRADE IN AVG-GRADE-DETAIL
           WRITE AVG-GRADE-DETAIL 

           MOVE "AVG-CS-GRADE: " TO AVG-GRADE-NAME
           MOVE AVG-CS-GRADE TO AVG-GRADE IN AVG-GRADE-DETAIL
           WRITE AVG-GRADE-DETAIL 

           CLOSE AVG-GRADE-FILE 
           
       .

       006-EXIT.
           EXIT.
